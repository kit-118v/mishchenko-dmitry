﻿using System.Collections.Generic;

namespace DotNetLab8.Models
{
    public interface IStudentRepository
    {
        void Add(Student item);
        IEnumerable<Student> GetAll();
        Student Find(long id);
        Student Remove(long id);
    }
}
