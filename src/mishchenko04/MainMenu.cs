﻿using System;

namespace DotNetLab4
{
    class Mainmenu
    {
        public static void Start()
        {
            var studentArray = new StudentArray();
            while (true)
            {
                PrintMainMenu();
                int choice;
                try
                {
                    choice = InputUtil.EnterInt("choice");
                    switch (choice)
                    {
                        case 1:
                            {
                                studentArray.AddStudent(CreateStudent());
                                break;
                            }
                        case 2:
                            {
                                IOSupport.PrintStudents(studentArray);
                                studentArray.DeleteStudentByIndex(InputUtil.EnterInt("index") - 1);
                                break;
                            }
                        case 3:
                            {
                                IOSupport.PrintStudents(studentArray);
                                break;
                            }
                        case 4:
                            {
                                studentArray.Sort();
                                break;
                            }
                        case 5:
                            {
                                IOSupport.PrintStudents(studentArray.SearchBySurname(InputUtil.EnterName("surname")));
                                break;
                            }
                        case 6:
                            {
                                var index = GetIndex(studentArray);    
                                var student = studentArray.GetStudent(index);
                                Console.WriteLine(student.ToString());
                                int edit = InputUtil.EnterInt("1 if you want to edit");
                                if (edit == 1)
                                {
                                    student = CreateStudent();
                                    studentArray.SetStudent(index, student);
                                } 
                                break;
                            }
                        case 7:
                            {
                                StudentFileIO.WriteList(studentArray);
                                break;
                            }
                        case 8:
                            {
                                string filename = null;
                                studentArray = StudentFileIO.ReadList(filename);
                                break;
                            }
                        case 9:
                            {
                                studentArray.Clear();
                                break;
                            }                     
                        case 0:
                            {              
                                
                                return;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public static void PrintMainMenu()
        {
            Console.WriteLine("Enter your choice:");
            Console.WriteLine("1. Add a student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Print list of students");
            Console.WriteLine("4. Sort list(by surname)");
            Console.WriteLine("5. Search students(by surname)");
            Console.WriteLine("6. Get student info by index");
            Console.WriteLine("7. Save list to file");
            Console.WriteLine("8. Load list from file");
            Console.WriteLine("9. Clear list");
            Console.WriteLine("0. Exit");
        }

        public static Student CreateStudent()
        {
            var student = new Student(InputUtil.EnterName("Surname"), 
                InputUtil.EnterName("Name"), 
                InputUtil.EnterName("Patronymic"), 
                InputUtil.EnterDate("DateOfBirth"), 
                InputUtil.EnterDate("EnterDate"), 
                InputUtil.EnterString("GroupIndex"), 
                InputUtil.EnterSentence("Faculty"), 
                InputUtil.EnterSentence("Specialty"), 
                InputUtil.EnterPercents("AcademicPerformance"));
            return student;
        }

        private static int GetIndex(StudentArray studentArray)
        {
            IOSupport.PrintStudents(studentArray);
            return InputUtil.EnterInt("index") - 1;
        }

    }
}
