﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DotNetLab4
{
    class StudentOperations
    {
        public static string GetStudentGroup(string faculty, string groupIndex)
        {
            var sb = new StringBuilder(IOSupport.AbbreviationFrom(faculty)).Append("-").Append(groupIndex);
            return sb.ToString();
        }

        public static string GetCourseAndSemester(DateTime enterDate)
        {
            var today = DateTime.Today;
            var years = YearDiff(today, enterDate);
            var course = years;
            int semester;
            if (today.Month > 9 || today.Month == 1)
            {
                course++;
                semester = 1;
            }
            else
            {
                semester = 2;
            }
            var sb = new StringBuilder();
            if (course <= 6)
            {
                sb = new StringBuilder("Course ").Append(course).Append(", semester ").Append(semester);
            }
            else
            {
                sb = new StringBuilder("Graduated");
            }
            return sb.ToString();
        }

        public static int GetStudentAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            return YearDiff(today, birthDate);
        }

        private static int YearDiff(DateTime firstDate, DateTime secondDate)
        {
            int diff;
            if (firstDate.CompareTo(secondDate) > 0) {
                diff = firstDate.Year - secondDate.Year;
                if (secondDate.Date > firstDate.AddYears(-diff)) diff--;
                return diff;
            }
            else
            {
                diff = secondDate.Year - firstDate.Year;
                if (firstDate.Date > secondDate.AddYears(-diff)) diff--;
                return diff;
            }
        }
    }
}