﻿using System;

namespace DotNetLab2
{
    class Mainmenu
    {
        public static void Start()
        {
            StudentArray studentArray = new StudentArray();
            while (true)
            {
                PrintMainMenu();
                int choice;
                try
                {
                    choice = InputUtil.EnterInt("choice");
                    switch (choice)
                    {
                        case 1:
                            {
                                studentArray.AddStudent(CreateStudent());
                                break;
                            }
                        case 2:
                            {
                                IOSupport.PrintStudents(studentArray);
                                studentArray.DeleteStudentByIndex(InputUtil.EnterInt("index") - 1);
                                break;
                            }
                        case 3:
                            {
                                IOSupport.PrintStudents(studentArray);
                                break;
                            }
                        case 4:
                            {
                                studentArray.Sort();
                                break;
                            }
                        case 5:
                            {
                                IOSupport.PrintStudents(studentArray.SearchBySurname(InputUtil.EnterName("surname")));
                                break;
                            }
                        case 6:
                            {
                                IOSupport.PrintStudents(studentArray);
                                int index = InputUtil.EnterInt("index") - 1;
                                Student student = studentArray.GetStudent(index);
                                Console.WriteLine(student.ToString());
                                break;
                            }
                        case 7:
                            {
                                studentArray.Clear();
                                break;
                            }
                        case 0:
                            {
                                return;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public static void PrintMainMenu()
        {
            Console.WriteLine("Enter your choice:");
            Console.WriteLine("1. Add a student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Print list of students");
            Console.WriteLine("4. Sort list(by surname)");
            Console.WriteLine("5. Search students(by surname)");
            Console.WriteLine("6. Get student info by index");
            Console.WriteLine("7. Clear list");
            Console.WriteLine("0. Exit");
        }

        public static Student CreateStudent()
        {
            var student = new Student();
            student.Surname = InputUtil.EnterName(nameof(student.Surname));
            student.Name = InputUtil.EnterName(nameof(student.Name));
            student.Patronymic = InputUtil.EnterName(nameof(student.Patronymic));
            student.DateOfBirth = InputUtil.EnterDate(nameof(student.DateOfBirth));
            student.EnterDate = InputUtil.EnterDate(nameof(student.EnterDate));
            student.GroupIndex = InputUtil.EnterString(nameof(student.GroupIndex));
            student.Faculty = InputUtil.EnterSentence(nameof(student.Faculty));
            student.Specialty = InputUtil.EnterSentence(nameof(student.Specialty));
            student.AcademicPerformance = InputUtil.EnterPercents(nameof(student.AcademicPerformance));
            return student;
        }

    }
}
