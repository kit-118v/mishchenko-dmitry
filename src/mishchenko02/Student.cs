﻿using System;

namespace DotNetLab2
{

    class Student
    {
        public string Surname { get; set; }
        public string Name { get; set; }
        public string Patronymic { get; set; }
        public DateTime DateOfBirth { get; set; }
        public DateTime EnterDate { get; set; }
        public string GroupIndex { get; set; }
        public string Faculty { get; set; }
        public string Specialty { get; set; }
        public int AcademicPerformance { get; set; }

        public Student() { }

        public Student(string surname, string name, string patronymic, DateTime dob, DateTime enterDate, string groupIndex, string faculty, string specialty, int academicPerformance)
        {
            Surname = surname;
            Name = name;
            Patronymic = patronymic;
            DateOfBirth = dob;
            EnterDate = enterDate;
            GroupIndex = groupIndex;
            Faculty = faculty;
            Specialty = specialty;
            AcademicPerformance = academicPerformance;
        }

        public override string ToString()
        {
            return string.Format("Surname: {0}\nName: {1}\nPatronymic: {2}\nDate of birth: {3}\nEnter date: {4}\nGroup index: {5}\nFaculty: {6}\nSpecialty: {7}\nAcademic performance: {8}\n",
                Surname, Name, Patronymic, DateOfBirth, EnterDate, GroupIndex, Faculty, Specialty, AcademicPerformance);
        }
    }
}
