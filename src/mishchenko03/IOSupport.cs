﻿using System;

namespace DotNetLab3
{
    class IOSupport
    {
        public static void PrintStudents(StudentArray students)
        {
            if (students != null)
            {
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("|  №  |     Surname     |      Name       |    Patronymic    |  Date of birth  |  Enter date  |  Group index  |            Faculty            |          Specialty        |    Academic performance   |");
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                int i = 0;
                foreach (Student student in students)
                {
                    String output = String.Format("| {0,-3} | {1,-15} | {2,-15} | {3,-16} | {4,-15} | {5,-12} | {6,-13} | {7,-29} | {8,-25} | {9,-25} |",
                        (++i), student.Surname, student.Name, student.Patronymic, student.DateOfBirth.ToString("dd.MM.yyyy"), student.EnterDate.ToString("dd.MM.yyyy"), student.GroupIndex, student.Faculty, student.Specialty, student.AcademicPerformance);
                    Console.WriteLine(output);
                }
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
            else
            {
                Console.WriteLine("Array is empty");
            }

        }


    }
}
