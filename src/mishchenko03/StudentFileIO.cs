﻿using System;
using System.IO;
using System.Globalization;

namespace DotNetLab3
{
    class StudentFileIO
    {
        private static readonly string DefaultFilename = @"db.txt";
        public static void WriteList(StudentArray students)
        {
            var file = new StreamWriter(DefaultFilename, false);
            foreach (Student student in students)
            {
                file.WriteLine(student.Surname);
                file.WriteLine(student.Name);
                file.WriteLine(student.Patronymic);
                file.WriteLine(student.DateOfBirth.ToString("dd MM yyyy"));
                file.WriteLine(student.EnterDate.ToString("dd MM yyyy"));
                file.WriteLine(student.GroupIndex);
                file.WriteLine(student.Faculty);
                file.WriteLine(student.Specialty);
                file.WriteLine(student.AcademicPerformance);
                file.WriteLine();
            }
            file.Close();
        }

        public static StudentArray ReadList(string filename)
        {
            try
            {
                if (filename == null) filename = DefaultFilename;
                var file = new StreamReader(filename);
                var students = new StudentArray();
                string line;
                int i = 1;
                while ((line = file.ReadLine()) != null)
                {
                    var student = new Student
                    {
                        Surname = line,
                        Name = file.ReadLine(),
                        Patronymic = file.ReadLine(),
                        DateOfBirth = DateTime.ParseExact(file.ReadLine(), "dd MM yyyy", CultureInfo.InvariantCulture),
                        EnterDate = DateTime.ParseExact(file.ReadLine(), "dd MM yyyy", CultureInfo.InvariantCulture),
                        GroupIndex = file.ReadLine(),
                        Faculty = file.ReadLine(),
                        Specialty = file.ReadLine(),
                        AcademicPerformance = Convert.ToInt32(file.ReadLine())
                    };
                    line = file.ReadLine();
                    if (line.Equals("")) Console.WriteLine(i++ + " students read");
                    students.AddStudent(student);
                };

                file.Close();

                return students;

            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured while reading file: " + e.Message);
            }

            return new StudentArray();
        }
    }
}
