﻿using System;
using System.Text;

namespace DotNetLab5
{
    public class StudentOperations
    {
        public delegate double Del(StudentArray studentArray);

        public static double CalculateAverageAge(StudentArray studentArray)
        {
           double sum = 0;

           foreach(var student in studentArray.Students)
           {
                sum += student.Age;
           }

           return sum / studentArray.Students.Length;
        }

        public static double CalculateAverageAcademicPerformance(StudentArray studentArray)
        {
            double sum = 0;

            foreach (var student in studentArray.Students)
            {
                sum += student.AcademicPerformance;
            }

            return sum / studentArray.Students.Length;
        }

        public static string GetStudentGroup(string faculty, string groupIndex)
        {
            var sb = new StringBuilder(IOSupport.AbbreviationFrom(faculty)).Append("-").Append(groupIndex);
            return sb.ToString();
        }

        public static string GetCourseAndSemester(DateTime enterDate)
        {
            var today = DateTime.Today;
            var years = YearDiff(today, enterDate);
            var course = years;
            int semester;
            if (today.Month > 9 || today.Month == 1)
            {
                course++;
                semester = 1;
            }
            else
            {
                semester = 2;
            }
            var sb = new StringBuilder();
            if (course <= 6)
            {
                sb = new StringBuilder("Course ").Append(course).Append(", semester ").Append(semester);
            }
            else
            {
                sb = new StringBuilder("Graduated");
            }
            return sb.ToString();
        }

        public static int GetStudentAge(DateTime birthDate)
        {
            var today = DateTime.Today;
            if (birthDate.CompareTo(today) > 0) return -1;      
            return YearDiff(today, birthDate);
        }

        public static StudentArray SearchBySurname(String surname,StudentArray studentArray)
        {
            var searched = new StudentArray();
            if (studentArray.Students != null)
            {
                foreach (Student student in studentArray.Students)
                {
                    if (student.Surname.ToLower().Contains(surname.ToLower()))
                    {
                        searched.AddStudent(student);
                    }
                }
                return searched;
            }
            return new StudentArray();
        }
        public static StudentArray Search(String predicate, StudentArray studentArray,int category)
        {
            var searched = new StudentArray();
            if (studentArray.Students != null)
            {
                switch (category) {

                    case 0:
                        {
                            foreach (Student student in studentArray.Students)
                            {
                                if (student.Group.ToLower().Contains(predicate.ToLower()))
                                {
                                    searched.AddStudent(student);
                                }
                            }
                            break;
                        }

                    case 1:
                        {
                            foreach (Student student in studentArray.Students)
                            {
                                if (student.Specialty.ToLower().Contains(predicate.ToLower()))
                                {
                                    searched.AddStudent(student);
                                }
                            }

                            break;
                        }
                    case 2:
                        {
                            foreach (Student student in studentArray.Students)
                            {
                                if (student.Faculty.ToLower().Contains(predicate.ToLower()))
                                {
                                    searched.AddStudent(student);
                                }
                            }

                            break;
                        }

                }        
                return searched;
            }
            return new StudentArray();
        }

        public static StudentArray RemoveStudents(StudentArray studentArray, StudentArray removing)
        {
            for(int i = 0; i < removing.Students.Length; i++)
            {
                for(int j = 0; j < studentArray.Students.Length; j++)
                {
                    if (studentArray.Students[j].Equals(removing.Students[i]))
                    {
                        studentArray.DeleteStudentByIndex(j);
                    }
                }
            }

            return studentArray;
        }

        private static int YearDiff(DateTime firstDate, DateTime secondDate)
        {
            int diff;
            if (firstDate.CompareTo(secondDate) > 0) {
                diff = firstDate.Year - secondDate.Year;
                if (secondDate.Date > firstDate.AddYears(-diff)) diff--;
                return diff;
            }
            else
            {
                diff = secondDate.Year - firstDate.Year;
                if (firstDate.Date > secondDate.AddYears(-diff)) diff--;
                return diff;
            }
        }
    }
}