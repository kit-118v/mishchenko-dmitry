using Microsoft.VisualStudio.TestTools.UnitTesting;
using DotNetLab5;
using System;
using System.Globalization;

namespace DotNetLab5Test
{
    [TestClass]
    public class StudentOperationsTest
    {
        private static readonly DateTime date = DateTime.ParseExact("01 01 2000", "dd MM yyyy", CultureInfo.InvariantCulture);
        private static readonly DateTime enterDate = DateTime.ParseExact("01 09 2018", "dd MM yyyy", CultureInfo.InvariantCulture);
        private static readonly DateTime dateInFuture = DateTime.ParseExact("01 01 2100", "dd MM yyyy", CultureInfo.InvariantCulture);
        private static readonly string filename = "test.xml";
        private static StudentArray studentArray;

        [AssemblyInitialize]
        public static void Init(TestContext context)
        { 
            Student a = new Student("a","a","a",date,enterDate,"118v","C","S",75);
            Student b = new Student("b","b","b",date,enterDate,"118v","C","S",80);
            Student c = new Student("c","c","c",date,enterDate,"118a","C","S",99);
            studentArray = new StudentArray();
            studentArray.AddStudent(a);
            studentArray.AddStudent(b);     
            studentArray.AddStudent(c);     
        }

        [TestMethod]
        public void ShouldCalculateAgeSuccessfully()
        {
            Assert.AreEqual(20, StudentOperations.GetStudentAge(date));
        }

        [TestMethod]
        public void ShouldNotCalculateAge()
        {
            Assert.AreNotEqual(100,StudentOperations.GetStudentAge(dateInFuture));
        }

        [TestMethod]
        public void ShouldCalculateGroupSuccessfully()
        {
            Assert.AreEqual("C-118v", StudentOperations.GetStudentGroup(studentArray.GetStudent(0).Faculty, studentArray.GetStudent(0).GroupIndex));
        }

        [TestMethod]
        public void ShouldSaveAndReadSuccessfully()
        {
            Serialization.SaveCollectionInXML(studentArray, filename);
            var actual = Serialization.LoadCollectionFromXML(filename);
            for(int i = 0; i < studentArray.Students.Length; i++)
            {
                Assert.AreEqual(studentArray.GetStudent(i),actual.GetStudent(i));
            }

        }

        [TestMethod]
        public void ShouldSearchByGroupSuccessfully()
        {
            Student a = new Student("a","a","a",date,enterDate,"118v","C","S",75);
            Student b = new Student("b","b","b",date,enterDate,"118v","C","S",80);
            Student c = new Student("c","c","c",date,enterDate,"118a","C","S",99);
            studentArray = new StudentArray();
            studentArray.AddStudent(a);
            studentArray.AddStudent(b);     
            studentArray.AddStudent(c);     
            var students = StudentOperations.Search("118a", studentArray, 0);
            Assert.AreEqual(studentArray.GetStudent(2), students.GetStudent(0));

        }

        [TestMethod]
        public void ShouldRemoveByGroupSuccessfully()
        {
            Student a = new Student("a", "a", "a", date, enterDate, "118v", "C", "S", 75);
            Student b = new Student("b", "b", "b", date, enterDate, "118v", "C", "S", 80);
            Student c = new Student("c", "c", "c", date, enterDate, "118a", "C", "S", 99);
            studentArray = new StudentArray();
            studentArray.AddStudent(a);
            studentArray.AddStudent(b);
            studentArray.AddStudent(c);
            studentArray = StudentOperations.RemoveStudents(studentArray, StudentOperations.Search("118v", studentArray, 0));
            Assert.AreEqual("c", studentArray.GetStudent(0).Surname);

        }


    }
}
