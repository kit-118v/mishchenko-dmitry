﻿using System;
using System.IO;
using System.Globalization;

namespace DotNetLab5
{
    class StudentFileIO
    {
        private static readonly string DefaultFilename = @"db.txt";
        public static void WriteList(StudentArray students)
        {
            var file = new StreamWriter(DefaultFilename, false);
            foreach (Student student in students)
            {
                file.WriteLine(student.Surname);
                file.WriteLine(student.Name);
                file.WriteLine(student.Patronymic);
                file.WriteLine(student.DateOfBirth.ToString("dd MM yyyy"));
                file.WriteLine(student.EnterDate.ToString("dd MM yyyy"));
                file.WriteLine(student.GroupIndex);
                file.WriteLine(student.Faculty);
                file.WriteLine(student.Specialty);
                file.WriteLine(student.AcademicPerformance);
                file.WriteLine();
            }
            file.Close();
        }

        public static StudentArray ReadList(string filename)
        {
            try
            {
                if (filename == null) filename = DefaultFilename;
                var file = new StreamReader(filename);
                var students = new StudentArray();
                string line;
                int i = 1;
                while ((line = file.ReadLine()) != null)
                {
                    var student = new Student(line, file.ReadLine(), file.ReadLine(),
                        DateTime.ParseExact(file.ReadLine(), "dd MM yyyy", CultureInfo.InvariantCulture), 
                        DateTime.ParseExact(file.ReadLine(), "dd MM yyyy", CultureInfo.InvariantCulture), 
                        file.ReadLine(), file.ReadLine(), file.ReadLine(), Convert.ToInt32(file.ReadLine()));         
                    line = file.ReadLine();
                    if (line.Equals("")) Console.WriteLine(i++ + " students read");
                    students.AddStudent(student);
                };

                file.Close();

                return students;

            }
            catch (Exception e)
            {
                Console.WriteLine("An error occured while reading file: " + e.Message);
            }

            return new StudentArray();
        }
    }
}
