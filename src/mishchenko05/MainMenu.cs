﻿using System;

namespace DotNetLab5
{
    class Mainmenu
    {
        public static void Start()
        {
            var studentArray = new StudentArray();
            while (true)
            {
                PrintMainMenu();
                int choice;
                try
                {
                    choice = InputUtil.EnterInt("choice");
                    switch (choice)
                    {
                        case 1:
                            {
                                studentArray.AddStudent(CreateStudent());
                                break;
                            }
                        case 2:
                            {
                                IOSupport.PrintStudents(studentArray);
                                studentArray.DeleteStudentByIndex(InputUtil.EnterInt("index") - 1);
                                break;
                            }
                        case 3:
                            {
                                PrintStudents();
                                int printChoice = InputUtil.EnterInt("printChoice");
                                switch (printChoice)
                                {
                                    case 0:
                                        {
                                            IOSupport.PrintStudents(studentArray);
                                            break;
                                        }
                                    case 1:
                                        {
                                            IOSupport.PrintStudents(StudentOperations.Search(InputUtil.EnterString("group"),studentArray,printChoice - 1));
                                            break;
                                        }
                                    case 2:
                                        {
                                            IOSupport.PrintStudents(StudentOperations.Search(InputUtil.EnterString("specialty"), studentArray, printChoice - 1));
                                            break;
                                        }
                                    case 3:
                                        {
                                            IOSupport.PrintStudents(StudentOperations.Search(InputUtil.EnterString("faculty"), studentArray, printChoice - 1));
                                            break;
                                        }
                                }
                               
                                break;
                            }
                        case 4:
                            {
                                studentArray.Sort();
                                break;
                            }
                        case 5:
                            {
                                IOSupport.PrintStudents(StudentOperations.SearchBySurname(InputUtil.EnterName("surname"), studentArray));
                                break;
                            }
                        case 6:
                            {
                                var index = GetIndex(studentArray);    
                                var student = studentArray.GetStudent(index);
                                Console.WriteLine(student.ToString());
                                int edit = InputUtil.EnterInt("1 if you want to edit");
                                if (edit == 1)
                                {
                                    student = CreateStudent();
                                    studentArray.SetStudent(index, student);
                                } 
                                break;
                            }
                        case 7:
                            {
                                StudentFileIO.WriteList(studentArray);
                                break;
                            }
                        case 8:
                            {
                                string filename = null;
                                studentArray = StudentFileIO.ReadList(filename);
                                break;
                            }
                        case 9:
                            {
                                studentArray.Clear();
                                break;
                            }
                        case 10:
                            {
                                Serialization.SaveCollectionInXML(studentArray, "db.xml");
                                break;
                            }
                        case 11:
                            {
                                studentArray = Serialization.LoadCollectionFromXML("db.xml");
                                break;
                            }
                        case 12:
                            {
                                RemoveStudents();
                                int deleteChoice = InputUtil.EnterInt("deleteChoice");
                                studentArray = StudentOperations.RemoveStudents(studentArray,StudentOperations.Search(InputUtil.EnterString("choice"),studentArray,deleteChoice));
                                break;
                            }
                        case 13:
                            {
                                AvgCalculateOperations();
                                int avgOperations = InputUtil.EnterInt("avgChoice"); ;
                                StudentOperations.Del del = null;
                                switch (avgOperations)
                                {
                                    case 0:
                                        {
                                            del = StudentOperations.CalculateAverageAge; 
                                            break;
                                        }
                                    case 1:
                                        {
                                            del = StudentOperations.CalculateAverageAcademicPerformance;
                                            break;
                                        }                                       
                                }

                                RemoveStudents();
                                int searchChoice = InputUtil.EnterInt("searchChoice");
                                Console.WriteLine(del?.Invoke(StudentOperations.Search(InputUtil.EnterString("choice"), studentArray, searchChoice)));
                                break;
                            }
                        case 0:
                            {

                                return;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }

        public static void PrintMainMenu()
        {
            Console.WriteLine("Enter your choice:");
            Console.WriteLine("1. Add a student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Print list of students");
            Console.WriteLine("4. Sort list(by surname)");
            Console.WriteLine("5. Search students(by surname)");
            Console.WriteLine("6. Get student info by index");
            Console.WriteLine("7. Save list to file");
            Console.WriteLine("8. Load list from file");
            Console.WriteLine("9. Clear list");
            Console.WriteLine("10. Save in XML");
            Console.WriteLine("11. Load from XML");
            Console.WriteLine("12. Remove by group");
            Console.WriteLine("13. Additional operations");
            Console.WriteLine("0. Exit");
        }

        public static void PrintStudents()
        {
            Console.WriteLine("0. All students");
            Console.WriteLine("1. By group");
            Console.WriteLine("2. By specialty");
            Console.WriteLine("3. By faculty");
        }

        public static void RemoveStudents()
        {
            Console.WriteLine("0. By group");
            Console.WriteLine("1. By specialty");
            Console.WriteLine("2. By faculty");
        }

        public static void AvgCalculateOperations()
        {
            Console.WriteLine("0. Average age");
            Console.WriteLine("1. Average academic performance");
        }

        public static Student CreateStudent()
        {
            var student = new Student(InputUtil.EnterName("Surname"), 
                InputUtil.EnterName("Name"), 
                InputUtil.EnterName("Patronymic"), 
                InputUtil.EnterDate("DateOfBirth"), 
                InputUtil.EnterDate("EnterDate"), 
                InputUtil.EnterString("GroupIndex"), 
                InputUtil.EnterSentence("Faculty"), 
                InputUtil.EnterSentence("Specialty"), 
                InputUtil.EnterPercents("AcademicPerformance"));
            return student;
        }

        private static int GetIndex(StudentArray studentArray)
        {
            IOSupport.PrintStudents(studentArray);
            return InputUtil.EnterInt("index") - 1;
        }

    }
}
