﻿using System;

namespace DotNetLab1
{
    class StudentArray
    {
        public Student[] Students { get; set; }
        public bool Empty { get; set; } = true;

        public void AddStudent(Student student)
        {
            if (Students == null)
            {
                Students = new Student[0];
            }
            var newArr = new Student[Students.Length + 1];
            for (int i = 0, length = Students.Length; i < length; i++)
            {
                newArr[i] = Students[i];
            }
            newArr[Students.Length] = student;
            Students = newArr;
            if (Empty && Students.Length > 0) Empty = false;
        }

        public void DeleteStudentByIndex(int index)
        {
            if (Students != null)
            {
                if (CheckIndex(index))
                {
                    var newArr = new Student[Students.Length - 1];
                    for (int i = 0; i < index; i++)
                    {
                        newArr[i] = Students[i];
                    }

                    for (int i = index + 1, length = Students.Length; i < length; i++)
                    {
                        newArr[i - 1] = Students[i];
                    }

                    Students = newArr;
                    if (!Empty && Students.Length == 0) Empty = true;

                }
                else
                {
                    Console.WriteLine("Index out of range");
                }
            }
            else
            {
                Console.WriteLine("Array is empty");
            }
        }

        private bool CheckIndex(int index)
        {
            return index >= 0 && index <= Students.Length - 1;
        }


        public void PrintStudents()
        {
            if (Students != null)
            {
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                Console.WriteLine("|  №  |     Surname     |      Name       |    Patronymic    |  Date of birth  |  Enter date  |  Group index  |            Faculty            |          Specialty        |    Academic performance   |");
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
                int i = 0;
                foreach (Student student in Students)
                {
                    string output = string.Format("| {0,-3} | {1,-15} | {2,-15} | {3,-16} | {4,-15} | {5,-12} | {6,-13} | {7,-29} | {8,-25} | {9,-25} |",
                        (++i), student.Surname, student.Name, student.Patronymic, student.DateOfBirth.ToString("dd.MM.yyyy"), student.EnterDate.ToString("dd.MM.yyyy"), student.GroupIndex, student.Faculty, student.Specialty, student.AcademicPerformance);
                    Console.WriteLine(output);
                }
                Console.WriteLine("-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
            }
            else
            {
                Console.WriteLine("Array is empty");
            }

        }

    }
}
