﻿using System;

namespace DotNetLab1
{
    class Mainmenu
    {
        public static void Start()
        {
            var studentArray = new StudentArray();
            while (true)
            {
                PrintMainMenu();
                int choice;
                try
                {
                    choice = InputUtil.EnterInt("choice");
                    switch (choice)
                    {
                        case 1:
                            {
                                studentArray.AddStudent(CreateStudent());
                                break;
                            }
                        case 2:
                            {
                                studentArray.PrintStudents();
                                studentArray.DeleteStudentByIndex(InputUtil.EnterInt("index") - 1);
                                break;
                            }
                        case 3:
                            {
                                studentArray.PrintStudents();
                                break;
                            }
                        case 0:
                            {
                                return;
                            }
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }



            }
        }

        public static void PrintMainMenu()
        {
            Console.WriteLine("Enter your choice:");
            Console.WriteLine("1. Add a student");
            Console.WriteLine("2. Remove student");
            Console.WriteLine("3. Print list of Sudents");
            Console.WriteLine("0. Exit");
        }

        public static Student CreateStudent()
        {
            var student = new Student();
            student.Surname = InputUtil.EnterName(nameof(student.Surname));
            student.Name = InputUtil.EnterName(nameof(student.Name));
            student.Patronymic = InputUtil.EnterName(nameof(student.Patronymic));
            student.DateOfBirth = InputUtil.EnterDate(nameof(student.DateOfBirth));
            student.EnterDate = InputUtil.EnterDate(nameof(student.EnterDate));
            student.GroupIndex = InputUtil.EnterString(nameof(student.GroupIndex));
            student.Faculty = InputUtil.EnterSentence(nameof(student.Faculty));
            student.Specialty = InputUtil.EnterSentence(nameof(student.Specialty));
            student.AcademicPerformance = InputUtil.EnterPercents(nameof(student.AcademicPerformance));
            return student;
        }

    }
}
